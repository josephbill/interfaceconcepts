package com.emobilis.interfaceconcepts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    TextView textView;
    TextInputEditText textInputEditText;
    //reference text to speech
    private TextToSpeech textToSpeech;
    private EditText mEditText;
    private SeekBar mSeekBarPitch;
    private SeekBar mSeekBarSpeed;
    private Button mButtonSpeak,btnWatcher;
    EditText editWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ref
        textView = findViewById(R.id.textDate);
        textInputEditText = findViewById(R.id.datePicker);
        mEditText = findViewById(R.id.edit_text);
        mSeekBarPitch = findViewById(R.id.seek_bar_pitch);
        mSeekBarSpeed = findViewById(R.id.seek_bar_speed);
        mButtonSpeak = findViewById(R.id.button_speak);
        editWatcher = findViewById(R.id.edit_text_username);
        btnWatcher = findViewById(R.id.btntextWatcher);


        //setting onclick to call my DatePickerFragment
        textInputEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //calling our date picker fragement
                //new instance of class
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });


        //calling the Calendar class //to create a new instance
        Calendar calendar = Calendar.getInstance();
        //string variable to hold the output from the calendar class
        String currentDate = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());

        Date currentTime = Calendar.getInstance().getTime();

        //set date to textview
        textView.append("Date is " + currentDate + currentTime);

        //text to speech
        textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    //set up the language for transaltion
                    //check if the language is not supported by the API
                    int result = textToSpeech.setLanguage(Locale.ENGLISH);

                    //check
                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Toast.makeText(MainActivity.this, "Language not supported", Toast.LENGTH_SHORT).show();
                    } else {
                        mButtonSpeak.setEnabled(true);
                    }

                } else {
                    //if the text to speech is not working give error
                    Log.e("TTS", "Initialization failed");
                    Toast.makeText(MainActivity.this, "Text to speech not working", Toast.LENGTH_SHORT).show();
                }
            }
        });


        //onclick for button
        mButtonSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speak();
            }
        });

        editWatcher.addTextChangedListener(textWatcher);

    }

    //text watcher concept
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
               String userWatcher = editWatcher.getText().toString().trim();
               //if text is changing in the input box enable the button watcher
               btnWatcher.setEnabled(!userWatcher.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    public void secondActivity(View v){
        Intent intent = new Intent(MainActivity.this,SecondActivity.class);
        startActivity(intent);
    }

    public void regex(View v){
        Intent intent = new Intent(MainActivity.this,RegexActivity.class);
        startActivity(intent);
    }

    private void beforeWorld() {
        Toast.makeText(this, "Text not yet inputted", Toast.LENGTH_SHORT).show();
    }

    private void helloWord() {
        Toast.makeText(this, "Text Watcher working", Toast.LENGTH_LONG).show();
    }


    //setting the pitch and speed of the speech
    private void speak() {
        //pick users input
        String text = mEditText.getText().toString();
        //select the level of pitch the user wants
        float pitch = (float) mSeekBarPitch.getProgress() / 50;
        //set the minimum pitch
        if (pitch < 0.1) pitch = 0.1f;
        //select the speed of the speech the user wants
        float speed = (float) mSeekBarSpeed.getProgress() / 50;
        //set the minimum speed
        if (speed < 0.1) speed = 0.1f;

        textToSpeech.setPitch(pitch);
        textToSpeech.setSpeechRate(speed);
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();  //create an instance of the Calendar class
        c.set(Calendar.YEAR, year); // set the year
        c.set(Calendar.MONTH, month); //set the month
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth); //set the day
        //giving the date format
        String currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
        //setting the date picked by user according to format to the edit text
        textInputEditText.setText(currentDateString);
    }

    @Override
    protected void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

}


