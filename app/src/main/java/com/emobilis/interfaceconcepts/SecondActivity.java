package com.emobilis.interfaceconcepts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {
    private static final String[] COUNTRIES = new String[]{
            "Afghanistan", "Albania", "Algeria", "Andorra", "Angola"
    };

    //email
    private EditText mEditTextTo;
    private EditText mEditTextSubject;
    private EditText mEditTextMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //fetching my arrays from strings.xml
        String[] countries = getResources().getStringArray(R.array.countries);
        //ref to my AutoComplete TextView
        AutoCompleteTextView editText = findViewById(R.id.actv);
        //reference an array to show list of suggestion in a custom layout
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.custom_item, R.id.text_view_list_item, countries);
        //set the edit text to the adapter
        editText.setAdapter(adapter);


        //find the views
        mEditTextTo = findViewById(R.id.edit_text_to);
        mEditTextSubject = findViewById(R.id.edit_text_subject);
        mEditTextMessage = findViewById(R.id.edit_text_message);

        Button buttonSend = findViewById(R.id.button_send);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMail();
            }
        });

    }
    //here i will choose a receipt and also the email client
    private void sendMail() {
        //pick up the recipients
        String recipientList = mEditTextTo.getText().toString();
        //separate the recipients using ,
        String[] recipients = recipientList.split(",");
        //pick the inputs for the subject and message
        String subject = mEditTextSubject.getText().toString();
        String message = mEditTextMessage.getText().toString();

        //launch the intent to send mail and also to choose client
        Intent intent = new Intent(Intent.ACTION_SEND);  //explicit intent call
        intent.putExtra(Intent.EXTRA_EMAIL, recipients); //put info to the intent
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        intent.setType("message/rfc822"); //accepting messages
        //allow the user to choose app to send email with
        startActivity(Intent.createChooser(intent, "Choose an email client"));


    }

}
