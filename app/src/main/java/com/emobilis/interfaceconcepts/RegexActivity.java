package com.emobilis.interfaceconcepts;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Pattern;

public class RegexActivity extends AppCompatActivity {
    //declare the pattern
    private static final Pattern PATTERN =
            Pattern.compile(
                    "^" +
                            "(?=.*[a-zA-Z])" +      //any letter
                            "(?=.*[@#$%^&+=~])"  +  //any special character
                            "(?=\\S+$)" +  //no whitespaces
                            ".{6,}" +               //at least 6 characters
                            "$"
            );

    private static final Pattern UserName  = Pattern.compile(
                    "(?=.*[a-zA-Z])"      //any letter

    );

    private TextInputLayout textInputEmail;
    private TextInputLayout textInputUsername;
    private TextInputLayout textInputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regex);

        textInputEmail = findViewById(R.id.text_input_email);
        textInputUsername = findViewById(R.id.text_input_username);
        textInputPassword = findViewById(R.id.text_input_password);

    }
    //regex pattern from the JAVA class Patterns
    private boolean validateEmail(){
        String emailInput = textInputEmail.getEditText().getText().toString().trim();
        if (emailInput.isEmpty()) {
            textInputEmail.setError("Field can't be empty");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            textInputEmail.setError("Please enter a valid email address");
            return false;
        } else {
            textInputEmail.setError(null);
            return true;
        }
    }
    //using custom regex
    private boolean validatePassword() {
        String passwordInput = textInputPassword.getEditText().getText().toString().trim();
        if (passwordInput.isEmpty()) {
            textInputPassword.setError("Field can't be empty");
            return false;
        } else if (!PATTERN.matcher(passwordInput).matches()){
            textInputPassword.setError("Password too weak");
            return false;
        } else {
            textInputPassword.setError(null);
            return true;
        }

    }

    //validation without regex
    private boolean validateUsername() {
        String usernameInput = textInputUsername.getEditText().getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputUsername.setError("Field can't be empty");
            return false;
        } else if (!UserName.matcher(usernameInput).matches()) {
            textInputUsername.setError("Username must be characters");
            return false;
        } else {
            textInputUsername.setError(null);
            return true;
        }
    }




    //on click for the button
    public void confirmInput(View v){
        //checking our validations
        if (!validateEmail() | !validateUsername() | !validatePassword()) {
            return;
        }

        //if the input matches the specific validations
        String input = "Email: " + textInputEmail.getEditText().getText().toString();
        input += "\n";
        input += "Username: " + textInputUsername.getEditText().getText().toString();
        input += "\n";
        input += "Password: " + textInputPassword.getEditText().getText().toString();
        Toast.makeText(this, input, Toast.LENGTH_SHORT).show();
    }


}
